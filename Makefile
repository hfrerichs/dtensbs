include make.inc

cmlite = libcmlite.so

cmlib       = cmlib-3.0
cmlib_src   = $(cmlib).tar.gz
cmlib_url   = https://math.nist.gov/cmlib

machcon = i1mach.f  d1mach.f
xerror  = fdump.f   j4save.f  xerabt.f  xerctl.f  xerprt.f  xerror.f  xerrwv.f  xersav.f  xgetua.f
dbsplin = dbintk.f  dbnfac.f  dbnslv.f  dbspvd.f  dbspvn.f  dbvalu.f  dintrv.f
dtensbs = db2ink.f  db2val.f  db3ink.f  db3val.f  dbknot.f  dbtpcf.f
fftpkg  = passb.f   passb2.f  passb3.f  passb4.f  passb5.f  passf.f   passf2.f  passf3.f  passf4.f  passf5.f  cfftb.f   cfftb1.f  cfftf.f   cfftf1.f  cffti.f   cffti1.f  radb2.f   radb3.f   radb4.f   radb5.f   radbg.f   radf2.f   radf3.f   radf4.f   radf5.f   radfg.f   rfftb.f   rfftb1.f  rfftf.f   rfftf1.f  rffti.f   rffti1.f
quadpkd = dqng.f    dqag.f    dqage.f   dqk51.f   dqk61.f   dqk21.f   dqk15.f   dqk31.f   dqk41.f   dqpsrt.f

sources = $(foreach src, $(machcon), $(cmlib)/src/machcon/$(src)) \
          $(foreach src, $(xerror),  $(cmlib)/src/xerror/$(src)) \
          $(foreach src, $(dbsplin), $(cmlib)/src/dbsplin/$(src)) \
          $(foreach src, $(dtensbs), $(cmlib)/src/dtensbs/$(src)) \
          $(foreach src, $(quadpkd), $(cmlib)/src/quadpkd/$(src))

r8sources = $(foreach src, $(fftpkg),  $(cmlib)/src/fftpkg/$(src))

objects = $(patsubst %.f, %.o, $(sources))

r8objects = $(patsubst %.f, %.o, $(r8sources))



.PHONY: all install uninstall clean veryclean
all: $(cmlite)


$(cmlite): $(cmlib) $(objects) $(r8objects)
	$(FC) -shared -o $(cmlite) $(objects) $(r8objects)


$(cmlib_src):
	wget $(cmlib_url)/$(cmlib_src)


$(cmlib): $(cmlib_src)
	tar -xvzf $(cmlib_src) $(sources) $(r8sources)
	patch -p0 < d1mach.patch
	patch -p0 < i1mach.patch
	patch -p0 < dtensbs.patch


%.o: %.f
	$(FC) $(CFLAGS) -c $< -o $@


$(r8objects): CFLAGS := $(R8FLAG) $(CFLAGS)


install:
	install -d $(LIBDIR)
	install -m 644 $(cmlite) $(LIBDIR)


uninstall:
	-rm -f $(LIBDIR)/$(cmlite)


clean:
	-rm -f $(objects) $(r8objects)


veryclean: clean
	-rm -rf $(cmlite)
